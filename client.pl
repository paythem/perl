=head1 Update: 2021-04-17
=pod
- 								WORK IN PROGRESS
- This is a programmatic example of a Perl implementation of the PayThem.Net API interface.
- Bump protocol version to 2.2.0.
- Officially:
  - added OpenSSL as encryption option. AES encryption to be moved to "Deprecated" state in later releases.
  - added the HASH_STUB to avoid HASH duplication when repeating calls without re-initializing object.
  - Server-side new calls (review official documentation version 2.2.0:
    - get_ProductAvailability: retrieve the availability for a single product.
    - get_AllProductAvailability: retrieve the availability for all products on profile.
    - get_ErrorDescription: retrieve description of error ID.
- Removing requirement to pass the APPID for constructor and set to default 2848.
- callAPI
  - Throws Exception if IV is passed, but openSSL module not installed as PHP module.
  - Throws Exception if IV is passed, but is not exactly 16 long.
- Several code refactoring, changes and optimizations.
- Added encryption method variables.
- Minimum PHP version bumped to 7.0.
- "Prettified" debug output.
- Add public class variables for cURL error and HTTP/S response code.
- Moved access to server URI to private variable.
- Only close cURL on destruct, allowing for multiple calls via single instantiated instance of object.
- Added destructor for GC.
- Fixed ENCRYPT_RESPONSE feature over openSSL.
- Completed PHPDoc comments.
- Parameters and return variable type definitions
=cut

use strict;
use warnings;
use POSIX 'strftime';
use HTTP::Headers;
use LWP::UserAgent;
use MIME::Base64;
use Digest::SHA qw/hmac_sha256_base64/;
use Digest::MD5 qw(md5 md5_hex);
use XML::Simple;
use Data::Dumper;
use DateTime;
use JSON;
use Crypt::AES::CTR;
use HTTP::Request::Common qw(POST);
use Digest::SHA qw(hmac_sha256_hex); 
use Net::Address::IP::Local;

$ENV{HTTPS_DEBUG} 						    = 1;

our %conf;
    $conf{'operator'}					    = "PayThem.Net";
    $conf{'currency'}					    = "";
    $conf{'URL'}						    = 'https://vvsdemo.paythem.net/API/2824/'; # Demo
    $conf{'PUBLIC_KEY'}					    = 'tkpafwqnquzwmiuxonhaupyboyzkuzbu';
    $conf{'PRIVATE_KEY'}				    = 'urfarjrdtjpntxypumkusjmdpftyqcun';
    $conf{'USERNAME'}					    = 'U07431';
    $conf{'PASSWORD'}					    = 'pjokpnmy';
    $conf{'API_VERSION'}				    = '2.2.0';
    $conf{'SERVER_URI'}					    = 'https://vvsdemo.paythem.net/API/2824/'; # Demo
    $conf{'SERVER_DEBUG'}				    = 'false';
    $conf{'FAULTY_PROXY'}				    = 'false';
    $conf{'ENCRYPT_RESPONSE'}			    = 'false';

#our $omethod                                = 'get_AllProductAvailability';
#our $omethod           			        = 'get_OEMList';
#our $omethod           			        = 'get_BrandList';
#our $omethod           			        = 'get_ProductList';
#our $omethod           			        = 'get_SalesTransaction_ByDateRange';
#our $omethod           			        = 'get_FinancialTransaction_ByDateRange';
#our $omethod           			        = 'get_ProductList';
#our $omethod           			        = 'get_Vouchers';
#our $omethod           			        = 'get_ProductAvailability';
our $omethod           			        = 'get_AllProductAvailability';
#our $omethod           			        = 'get_ErrorDescription';

our %all_paramameters 				        = (
    "PRODUCT_ID"				        	, 1,
    "QUANTITY"						        , 1
);


sub local_get_config {
	my $value							    = shift;
	return($conf{$value});
}

sub log {
	my $message							    = shift;
	my $time							    = localtime;
	my $pid								    = $$;
	my $operator						    = &local_get_config('operator');
	warn "$time : ($pid) $operator: $message\n";
	return 1;
}

##################### Internal Subroutines. ##################
sub http_request {
	my $data							= shift;
	my $URL								= &local_get_config('URL');
	my $USERNAME						= &local_get_config('USERNAME');
	my $PASSWORD						= &local_get_config('PASSWORD');
	my $PUBLIC_KEY						= &local_get_config('PUBLIC_KEY');
	my $PRIVATE_KEY						= &local_get_config('PRIVATE_KEY');
	my $API_VERSION						= &local_get_config('API_VERSION');
	my $SERVER_URI						= &local_get_config('SERVER_URI');
	my $SERVER_DEBUG					= &local_get_config('SERVER_DEBUG');
	my $FAULTY_PROXY					= &local_get_config('FAULTY_PROXY');
	my $ENCRYPT_RESPONSE				= &local_get_config('ENCRYPT_RESPONSE');
	my $SERVER_TIMESTAMP				= strftime "%Y-%m-%d %H:%M:%S ", localtime;
	my $SERVER_TIMEZONE					= strftime "%Z", localtime;
	my $SOURCE_IP						= Net::Address::IP::Local->public;

	my %rdata_hash 						= (
		'API_VERSION'					=> $API_VERSION,
		'SERVER_URI'					=> $SERVER_URI,
		'SERVER_DEBUG'					=> $SERVER_DEBUG,
		'FAULTY_PROXY'					=> $FAULTY_PROXY,
		'USERNAME'						=> $USERNAME,
		'PASSWORD'						=> $PASSWORD,
		'PUBLIC_KEY'					=> $PUBLIC_KEY,
		'SOURCE_IP'						=> $SOURCE_IP,
		'SERVER_TIMESTAMP'				=> $SERVER_TIMESTAMP,
		'SERVER_TIMEZONE'				=> $SERVER_TIMEZONE,
		'ENCRYPT_RESPONSE'				=> 'true',
		'FUNCTION'						=> $omethod,
		'HASH_STUB'                     => int(rand(1000000)),
		'PARAMETERS'					=> \%all_paramameters
	);
	my $content							= encode_json \%rdata_hash;
	my $hmac							= hmac_sha256_hex($content, $PRIVATE_KEY);
	&log("\n----------REQUEST X-Public-Key: \n" . $PUBLIC_KEY);
	&log("\n----------REQUEST X-Hash:	\n" . $hmac);
	&log("\n----------REQUEST X-Sourceip:	\n" . $SOURCE_IP);
	my $req								= POST $URL.$omethod, [
		PUBLIC_KEY						=> &local_get_config('PUBLIC_KEY'),
		CONTENT							=> doEncrypt($content),
	];
	$req->header('X-Public-Key'			=> $PUBLIC_KEY );
	$req->header('X-Hash'				=> $hmac );
	$req->header('X-Sourceip'			=> $SOURCE_IP );
	my $ua								= LWP::UserAgent->new();
	$ua->credentials($URL, 'Transfer-To', $USERNAME, $PASSWORD);
	$ua->agent("Transfer-To");
	$ua->timeout(300);
	$ua->ssl_opts('verify_hostname'	    => 0);
	my $response						= $ua->request($req);

	if ( $response->is_error() ){
		print "Error-Code : "	        , $response->code()	, "\n";
		print "Error-Message : "        , $response->message()	, "\n";
	} else {
		my $res 						= $response->content();
		print $res, "\n";
	}
	return $response;
}

sub doEncrypt {
	my $content							= shift;
	my $ciphertext						= Crypt::AES::CTR::encrypt($content, &local_get_config('PRIVATE_KEY'), 256);
	return encode_base64($ciphertext);
}

sub doDecrypt{
    my $content                         = shift;
    my $unCipheredText                  = decode_base64(Crypt::AES::CTR::encrypt($content, &local_get_config('PRIVATE_KEY'), 256));
    return $unCipheredText;
}

##################### END Internal Subroutines. ##############
http_request();
1;